import java.util.*;
/**
 * Quaternions. Basic operations.
 */
public class Quaternion {

    // TODO!!! Your fields here!
    private double a, b, c, d;
    //private double[] q_parts = new double[4];
    private static final double threshold = 0.000001;

    /**
     * Constructor from four double values.
     *
     * @param a real part
     * @param b imaginary part i
     * @param c imaginary part j
     * @param d imaginary part k
     */
    public Quaternion(double a, double b, double c, double d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }


    /**
     * Real part of the quaternion.
     *
     * @return real part
     */
    //2
    public double getRpart() {
        return this.a;
    }


    /**
     * Imaginary part i of the quaternion.
     *
     * @return imaginary part i
     */
    //3
    public double getIpart() {
        return this.b;
    }


    /**
     * Imaginary part j of the quaternion.
     *
     * @return imaginary part j
     */
    //4
    public double getJpart() {
        return this.c;
    }

    /**
     * Imaginary part k of the quaternion.
     *
     * @return imaginary part k
     */
    //5
    public double getKpart() {
        return this.d;
    }

    /**
     * Conversion of the quaternion to the string.
     *
     * @return a string form of this quaternion:
     * "a+bi+cj+dk"
     * (without any brackets)
     */
//   //https://coderoad.ru/27469858/%D0%A0%D0%B0%D0%B7%D0%B1%D0%BE%D1%80-%D1%81%D1%82%D1%80%D0%BE%D0%BA%D0%B8-%D1%81-%D0%BF%D0%BE%D0%BC%D0%BE%D1%89%D1%8C%D1%8E-%D1%82%D0%BE%D0%BA%D0%B5%D0%BD%D0%B8%D0%B7%D0%B0%D1%82%D0%BE%D1%80%D0%B0-%D0%B8-%D0%BF%D0%B5%D1%80%D0%B5%D0%B7%D0%B0%D0%BF%D0%B8%D1%81%D1%8C-toString
    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        try {
            sb.append(a);

            if (b >= 0) {
                sb.append("+" + b + "i");
            } else {
                sb.append("" + b + "i");
            }
            if (c >= 0) {
                sb.append("+" + c + "j");
            } else {
                sb.append("" + c + "j");
            }
            if (d >= 0) {
                sb.append("+" + d + "k");
            } else {
                sb.append("" + d + "k");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();

    }


    /**
     * Conversion from the string to the quaternion.
     * Reverse to <code>toString</code> method.
     *
     * @param s string of form produced by the <code>toString</code> method
     * @return a quaternion represented by string s
     * @throws IllegalArgumentException if string s does not represent
     *                                  a quaternion (defined by the <code>toString</code> method)
     */

    public static Quaternion valueOf(String s) throws IllegalArgumentException {
        if (!s.matches("^.*i.*j.*k$")) {
            throw new IllegalArgumentException("There are missing quaternion parts or the order is not valid: " + s);
        }
        if (s.matches(".*[ijk]{2,}.*")) {
            throw new IllegalArgumentException("is not valid quaternion: " + s);
        }

        try {

            String s1 = s.replace("-", " -");
            // s1 = s1.replace("+", " +");
            String s2 = s1.replace("e -", "e-");
            if (s2.charAt(0) == ' ') s2 = s2.substring(1);
            String[] parts = s2.split("[ +ijk]+");
            if  (parts.length != 4) {
                throw new IllegalArgumentException("is not valid quatenion: " + s);
            }
            Double o, i, j, k;

            o = Double.valueOf(parts[0]);
            i = Double.valueOf(parts[1]);
            j = Double.valueOf(parts[2]);
            k = Double.valueOf(parts[3]);




            return new Quaternion(o, i, j, k);
        } catch (Exception e) {
            throw new IllegalArgumentException("Illegal input '" + s + "'");
        }
    }

    /**
     * Clone of the quaternion.
     *
     * @return independent clone of <code>this</code>
     */
    //8
    @Override
    public Object clone() throws CloneNotSupportedException {
        try {
            return new Quaternion(a, b, c, d);
        } catch (Exception evv) {
            throw new CloneNotSupportedException();
        }
    }

    /**
     * Test whether the quaternion is zero.
     *
     * @return true, if the real part and all the imaginary parts are (close to) zero
     */
    public boolean isZero() {
        if (Math.abs(this.a) <= threshold && Math.abs(this.b) <= threshold && Math.abs(this.c) <= threshold && Math.abs(this.d) <= threshold)
            return true;
        return false;
    }
//        for (int i = 0; i < this.q_parts.length; i++) {
//
//            if (Math.abs(this.q_parts[i]) >= threshold) return false;
//        }
//
//        return true;
//    }

    /**
     * Conjugate of the quaternion. Expressed by the formula
     * conjugate(a+bi+cj+dk) = a-bi-cj-dk
     *
     * @return conjugate of <code>this</code>
     */
    public Quaternion conjugate() {
        return new Quaternion(a, -b, -c, -d);

    }


    /**
     * Opposite of the quaternion. Expressed by the formula
     * opposite(a+bi+cj+dk) = -a-bi-cj-dk
     *
     * @return quaternion <code>-this</code>
     */

    public Quaternion opposite() {
        return new Quaternion(-a, -b, -c, -d);
    }

    /**
     * Sum of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
     *
     * @param q addend
     * @return quaternion <code>this+q</code>
     */
    public Quaternion plus(Quaternion q) {
        return new Quaternion(this.a + q.a, this.b + q.b, this.c + q.c, this.d + q.d);
    }

    /**
     * Product of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
     * (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
     *
     * @param q factor
     * @return quaternion <code>this*q</code>
     */
    public Quaternion times(Quaternion q) {
        double a1, b1, c1, d1;
        a1 = this.a * q.a - this.b * q.b - this.c * q.c - this.d * q.d;
        b1 = this.a * q.b + this.b * q.a + this.c * q.d - this.d * q.c;
        c1 = this.a * q.c - this.b * q.d + this.c * q.a + this.d * q.b;
        d1 = this.a * q.d + this.b * q.c - this.c * q.b + this.d * q.a;

        return new Quaternion(a1, b1, c1, d1);
    }

    /**
     * Multiplication by a coefficient.
     *
     * @param r coefficient
     * @return quaternion <code>this*r</code>
     */
    public Quaternion times(double r) {
        return new Quaternion(this.a * r, this.b * r, this.c * r, this.d * r);
    }

    /**
     * Inverse of the quaternion. Expressed by the formula
     * 1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
     * ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
     *
     * @return quaternion <code>1/this</code>
     */
    public Quaternion inverse() {
        if (this.isZero())
            throw new RuntimeException("Cannot inverse a zro quaterion");
        double divisor = Math.pow(a, 2) + Math.pow(b, 2) + Math.pow(c, 2) + Math.pow(d, 2);

        return new Quaternion(a / divisor, -b / divisor, -c / divisor, -d / divisor);


    }

    /**
     * Difference of quaternions. Expressed as addition to the opposite.
     *
     * @param q subtrahend
     * @return quaternion <code>this-q</code>
     */
    public Quaternion minus(Quaternion q) {
        return this.plus(q.opposite());
    }

    /**
     * Right quotient of quaternions. Expressed as multiplication to the inverse.
     *
     * @param q (right) divisor
     * @return quaternion <code>this*inverse(q)</code>
     */
    public Quaternion divideByRight(Quaternion q) {
        if (q.isZero())
            throw new ArithmeticException("Wrong argument " + q);
        return this.times(q.inverse());
    }

    /**
     * Left quotient of quaternions.
     *
     * @param q (left) divisor
     * @return quaternion <code>inverse(q)*this</code>
     */
    public Quaternion divideByLeft(Quaternion q) {
        if (q.isZero())
            throw new ArithmeticException("Wrong argument " + q);
        return q.inverse().times(this);
    }

    /**
     * Equality test of quaternions. Difference of equal numbers
     * is (close to) zero.
     *
     * @param qo second quaternion
     * @return logical value of the expression <code>this.equals(qo)</code>
     */
    @Override
    public boolean equals(Object qo) {
        Quaternion temp;
        try {
            temp = (Quaternion) qo;
        } catch (Exception e) {
            throw new IllegalArgumentException("Input is not of type Quaternion" + qo);
        }

//        for (int i =0; i < this.q_parts.length; i++){
//            if (Math.abs(temp.q_parts[i]  - this.q_parts[i])  > threshold)
//                return false;
//        }return true;

        if (Math.abs(temp.a - this.a) < threshold && Math.abs(temp.b - this.b) < threshold && Math.abs(temp.c - this.c) < threshold && Math.abs(temp.d - this.d) < threshold)
            return true;
        return false;
    }


    /**
     * Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
     *
     * @param q factor
     * @return dot product of this and q
     */
    public Quaternion dotMult(Quaternion q) {
        Quaternion q_1 = this.times(q.conjugate());
        Quaternion q_2 = q.times(this.conjugate());
        return q_1.plus(q_2).times(0.5);
    }

    /**
     * Integer hashCode has to be the same for equal objects.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        int prime = 31;
        return prime * prime * prime * prime * (int) a + prime * prime * prime * (int) b + prime * prime * (int) c + prime * (int) d;
    }

    /**
     * Norm of the quaternion. Expressed by the formula
     * norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
     *
     * @return norm of <code>this</code> (norm is a real number)
     */
    public double norm() {
        return Math.sqrt(Math.pow(a, 2) +
                Math.pow(b, 2) +
                Math.pow(c, 2) +
                Math.pow(d, 2));
    }

    /**
     * Main method for testing purposes.
     *
     * @param arg command line parameters
     */
    public static void main(String[] arg) {
//
//        System.out.println (valueOf("2+3i+4j+5k"));
//        System.out.println (valueOf("-2-3i-4j-5k"));
//        System.out.println (valueOf("-2.0e-3-4.0e-1i-5.0e-2j-6.0e-3k"));
//        System.out.println (valueOf("     "));
//        System.out.println (valueOf("2+3i+4j+5k+"));
//        System.out.println (valueOf("2+3i+4j+5kk"));
        System.out.println (valueOf("2+3j+4i+5k"));

    }
}
